+++
title = "Développement logiciel"
+++

Dix ans d'expérience d'ingénierie logicielle. Nous avons livré des logiciels en production dans plusieurs languages et technologies: Python et Django, Javascript et Vue.js ou Angular.js, Common Lisp, PHP et Laravel, pour de grosses entreprises, l'administration française ou des particuliers.


<!--more-->

Nous pouvons développer des logiciels avec une interface en ligne de commande, avec une interface graphique multi-plateforme, et avec une interface web.

Nous pouvons créer une base de données ou nous interfacer avec une existante. Nous pouvons utiliser des base de données relationnelles comme MySQL, PostGres ou SQLite ainsi que des bases de données non relationnelles (Mongo, Tarantool,…).

Nous pouvons utiliser des écosystèmes logiciels riches en fonctionnalités (Python, Django, PHP et Laravel…) ou utiliser des solutions plus efficientes (Lisp). Nous n'avons rien contre Elixir ou Clojure.

Nous développons des sites web interactifs (VueJS), avec une interface "responsive", pouvant s'interfacer avec une base de données.